﻿<?php
/*
Plugin Name: WP-Fest admin
Plugin URI: http://wp-ermak.ru/
Description: mainframe for WP-Fest family
Version: 0.0.1
Author: Genagl
Author URI: http://wp-ermak.ru/author
License: GPL2
Text Domain:   wpfa
Domain Path:   /lang/
*/
/*  Copyright 2018  Genagl  (email: genag1@list.ru)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/ 

//библиотека переводов
function init_textdomain_wpfa() 
{ 
	if (function_exists('load_plugin_textdomain')) 
	{
		load_plugin_textdomain("wpfa", false , dirname( plugin_basename( __FILE__ ) ) .'/lang/');     
	} 
}
add_action('plugins_loaded', 'init_textdomain_wpfa');

//Paths
define('WPFA_URLPATH', WP_PLUGIN_URL.'/wp_fest_admin/');
define('WPFA_REAL_PATH', WP_PLUGIN_DIR.'/'.plugin_basename(dirname(__FILE__)).'/');
define('_REAL_PATH', WPFA_REAL_PATH);
define('WPFA', 'wpfa');

require_once(WPFA_REAL_PATH.'class/WPFA.class.php');

if (function_exists('register_activation_hook'))
{
	register_activation_hook( __FILE__, [ WPFA, 'activate' ] );
}

if (function_exists('register_deactivation_hook'))
{
	register_deactivation_hook(__FILE__, array(WPFA, 'deactivate'));
}

add_filter("init", function()
{
	WPFA::get_instance();
}, 1);